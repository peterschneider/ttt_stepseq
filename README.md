This app has been written after watching the following lecture from Joachim Breitner: https://www.joachim-breitner.de/blog/679-Tiptoi_auf_der_GPN_15

It is a simple implementation of a 16-step step sequencer. Its app OID is 44, so select 44 to load it.

First select one of the samples 4821-4831 (see below for a list), then select the slots where the sample should be placed. You can select multiple slots if you wish, they identified by OIDs 4761 through 4776. A short positive sound confirms that the sample has been saved.

With 4806 you can play the currently selected sample (debugging).

You can start playback with 4716 (no repetition), 4717 (5 loops) or 4718 (10 loops).

Samples:

* 4821 silence
* 4822 bass
* 4823 bass 2
* 4824 snare
* 4825 click
* 4826 plop
* 4827 plop 2
* 4828 roadrunner
* 4829 trill
* 4830 tsch
* 4831 woop woop

![ids.png](https://bitbucket.org/repo/eMLr4r/images/3400622616-ids.png)

Also posted at http://www.raketenwerfer.de/post/120870754563/tttool-step-sequencer